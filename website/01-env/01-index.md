---
layout: page
title: JavaScript Development Environment
permalink: /env/
---

# JavaScript Development Environment

<br/>

<a href="/env/docker/">Docker</a> || <a href="/env/atom/">Atom</a> || <a href="/env/git/">Git</a>

<br/>

I'm working on ubuntu linux 14.04 LTS with Docker and my IDE is Atom with some plugins. No more anything need for comfort development.


<br/><br/>


For develop app in the browser, without anything else, you can use c9.io service.


<br/>

### Video Courses about environment:

<br/>

**[Pluralsight] Building a JavaScript Development Environment**

https://github.com/coryhouse/javascript-development-environment

<br/>

And additionally. I very interesting in CoreOS as platform for docker container. As distributed platform for multiple docker containers. For study CoreOS, I watched course:


**[O'Reilly Media / Infinite Skills] Introduction to CoreOS Training Video [2015, ENG]**




<br/>

### Building products with javascript

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/gSwLKyVnGTw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

<br/>

### Minimal Express Server with Nginx and PM2

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/N6ha6i8jt_8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

<br/>

**Code:** <br/>
https://github.com/yongzhihuang/PentaCode/tree/master/minimalExpressNginxPm2Kit


<br/>

**How To Set Up a Node.js Application for Production on Ubuntu 16.04**

https://gist.github.com/tomysmile/9c329a40dbd7da917d52a4886fd32fc3


<br/>

<a href="/env/bower/">bower</a> (Deprecated)