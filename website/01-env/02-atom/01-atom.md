---
layout: page
title: Atom (IDE)
permalink: /env/atom/
---

# Atom (IDE)

<br/>

### [Traversy Media] My Top 5 Free Text Editors For Web Development


<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/AJnhqf5IRC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>


<br/>

### 10 Essential Atom Editor Packages & Setup


<div align="center">

<iframe width="853" height="480" src="https://www.youtube.com/embed/aiXNKHKWlmY" frameborder="0" allowfullscreen></iframe>

</div>


<br/>

### Atom Editor Tutorials


<div align="center">

<iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLLnpHn493BHHf0w8uGu9NM8LPf498ZvL_" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

### [Install Atom on Ubuntu 14.04](//jsdev.org/env/ide/atom/install-atom-on-ubuntu-14-04/)  
