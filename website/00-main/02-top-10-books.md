---
layout: page
title: Top books for javascript (frameworks) development
permalink: /top-10-books/
---


# Top books for javascript (frameworks) development

<br/>

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/getting-mean-with-mongo-express-angular-and-node" rel="nofollow">Getting MEAN with Mongo, Express, Angular, and Node</a></li>
</ul>


<br/>
<hr/>

<br/>

### Plan to read

<br/>

<ul>
    <li>David Gonzalez - Developing Microservices with Node.js - 2016</li>
</ul>
