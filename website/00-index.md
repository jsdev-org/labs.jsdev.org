---
layout: page
title: JavaScript Development with modern Technologies
permalink: /
---

# JavaScript Development with modern Technologies

<br/>

### Plans

- <a href="https://github.com/marley-nodejs/Server-Side-Rendering-with-React-and-Redux" rel="nofollow">~~[Stephen Grider] Server Side Rendering with React and Redux [Udemy, ENG, 2017]~~</a>

- <a href="https://github.com/marley-nodejs/Advanced-React-and-Redux-2018-Edition" rel="nofollow">~~[Stephen Grider] Advanced React and Redux: 2018 Edition [Udemy, ENG, 2018]~~</a>

- <a href="https://github.com/marley-nodejs/The-Modern-GraphQL-Bootcamp" rel="nofollow">~~[Andrew Mead] The Modern GraphQL Bootcamp (Advanced Node.js) [2018, ENG]~~</a>

- <a href="https://github.com/marley-nodejs/Full-Stack-React-with-GraphQL-and-Apollo-Boost" rel="nofollow">~~[Udemy, Reed Barger] Full-Stack React with GraphQL and Apollo Boost [2018, ENG]~~</a>

- <a href="https://github.com/marley-nodejs/Docker-and-Kubernetes-The-Complete-Guide" rel="nofollow">~~[Stephen Grider] Docker and Kubernetes: The Complete Guide [2018, ENG]~~</a>

* <a href="https://github.com/marley-nodejs/Ethereum-and-Solidity-The-Complete-Developers-Guide" rel="nofollow">[Stephen Grider] Ethereum and Solidity: The Complete Developer's Guide [2018, ENG]</a>

* <a href="https://github.com/marley-nodejs/Master-Full-Stack-Web-Development-Node-SQL-React-and-More" rel="nofollow">[David Joseph Katz] Master Full-Stack Web Development | Node, SQL, React, &amp; More [ENG, 2018]</a>

* [Reed Barger] Universal React with Next.js - The Ultimate Guide [2018, ENG]

* [Stephen Grider] Machine Learning with Javascript [2018, ENG]

* [Tim Ermilov] - Exploring Docker images optimizations for Node.js [2018, ENG] https://www.youtube.com/watch?v=VF_UeW_l_rY

* [Stephen Grider] Electron for Desktop Apps: The Complete Developer's Guide [2017, ENG]

* [Stephen Grider] Node JS: Advanced Concepts [2018, ENG]

* [Stephen Grider] The Complete React Native and Redux Course [ENG]

* [Stephen Grider] React Native: Advanced Concepts [ENG]

<br/>

**and maybe later**

- [Traversy Media
  ] Build a Node.js App With Sequelize [2018, ENG] - https://www.youtube.com/watch?v=bOHysWYMZM0

- [Stephen Grider] The Complete Developers Guide to MongoDB [ENG]

- [Stephen Grider] GraphQL with React: The Complete Developers Guide [ENG]

- [Udemy] Design Patterns with JavaScript ES5/6 and Node.js|From zero [ENG]

- [Udemy] React - Mastering Test Driven Development [ENG]

- [Packtpub] Professional Node.js

- Pluralsight - API Design in Node.js Featuring Express Mongo

- Learn How to Use Closures in Javascipt | Eduonix https://www.youtube.com/watch?v=NTOyWEk3pnM

- Learning Microservices With Express.js & MongoDB - https://www.youtube.com/watch?v=gF8IYisXByw&list=PLDmvslp_VR0xZGhJHMjy5dozCDJYZK6W-
- BxJS - Building a high-availability chat with Node.js, Redis and Docker - https://www.youtube.com/watch?v=shJo8Wj7jMA
- Simple Machine Learning With JavaScript - Brain.js - https://www.youtube.com/watch?v=RVMHhtTqUxc
- [Lynda.com Sasha Vodnik] JavaScript: Closures [2018, ENG]
- [Lynda] Node.js: Design Patterns [2018, ENG]

- <a href="https://github.com/planetoftheweb/learnangular5" rel="nofollow">~~[Lynda, Ray Villalobos] Learning Angular [2018, ENG]~~</a>
- Blockchain Development With NodeJS (https://www.youtube.com/watch?v=av9HRXEVDWc)
- Learn Angular 5 in 30 minutes Eduonix (https://www.youtube.com/watch?v=_tEZgpmxKck)
- Machine Learning Tutorial for Beginners - USING JAVASCRIPT! (https://www.youtube.com/watch?v=9Hz3P1VgLz4&t=0s)

<br/>

**and watch again**

<br/>

- ~~Docker for Web Developers [2016, ENG]~~

- <a href="https://github.com/marley-nodejs/learning-full-stack-javascript-development-mongodb-node-and-react" rel="nofollow">~~[Samer Buna] Learning Full-Stack JavaScript Development: MongoDB, Node and React [2016, ENG]~~</a>
- ~~Creating Apps With Angular, Node, and Token Authentication [2014, ENG]~~

<br/>

### Oracle MOOC: Soar higher with Oracle JavaScript Extension Toolkit (JET) 4.0 (2018)

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/jiZh6ERmRkY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

<a href="https://apexapps.oracle.com/pls/apex/f?p=44785:149:0::::P149_EVENT_ID:5725">Link</a>

<br/>

### Oracle MOOC: Introduction to NodeJS Using Oracle Cloud (2018)

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/vNzuEYxsVyg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

<br/>

**This video course can help to start working with node.js with oracle db**

<a href="/backend/nodejs/2018/introduction-to-nodejs-using-oracle-cloud/">Link</a>
